<?php
namespace tinyeditor;
class Controller_Grid_Format_tinyeditor extends \AbstractController {
	function initField() {
		// add add-on locations to pathfinder
		$l = $this->api->locate('addons', __NAMESPACE__, 'location');
		$addon_location = $this->api->locate('addons', __NAMESPACE__);
		$this->api->pathfinder->addLocation($addon_location, array(
			'js' => 'js',
		))->setParent($l);
		// Load, but not init tinymce so it is available if we need to use it in CRUD or any other Ajax form
		$this->api->jui->addStaticInclude('tinymce/tinymce.min');
	}
	
	function formatField($field) {
		$this->owner->current_row_html[$field] = 'Hide this field in grids or extend this controller to show it your way.';
	}
	
}