ATK4 TINYMCE EDITOR ADDON WITH INTEGRATED FILE UPLOAD
by Jaume Martinez (@jaumemartinez / http://tiktakwebs.com).

Usage is very easy:

tinyeditor folder should be under atk4-addons folder.

In your model you only have to set:

$this->addField('content')
			->type('text')
			->display('tinyeditor/tinyeditor');
			
That's all!!
			
js folder contains Tinymce Editor and free filemanager plugin from http://test.albertoperipolli.com/filemanager4tinymce/

You have to edit js/tinymce/plugins/filemanager/config.php to set the upload and thumbnail folders.